from django.urls import path
from . import views

from django.urls import path

from . import views

urlpatterns = [
        #Leave as empty string for base url
	path('', views.store, name="store"),
	path('cart/', views.cart, name="cart"),
	path('checkout/', views.checkout, name="checkout"),
    path('review/', views.review, name="review"),
	path('update_item/', views.updateItem, name="update_item"),
	path('process_order/', views.processOrder, name="process_order"),
	path('review/', views.review,name="review"),
	path('review/<int:pk>/', views.reviews, name="reviews"),
	path('history/',views.history,name='history'),
	path('signin/',views.log_in,name='signin'),
	path('signup/',views.signup,name='signup'),
	path('signout/', views.signout,name='signout'),
]